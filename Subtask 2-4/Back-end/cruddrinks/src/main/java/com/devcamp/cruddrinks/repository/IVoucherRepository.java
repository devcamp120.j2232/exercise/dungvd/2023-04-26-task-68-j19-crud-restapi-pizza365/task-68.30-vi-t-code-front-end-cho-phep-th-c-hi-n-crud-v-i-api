package com.devcamp.cruddrinks.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.cruddrinks.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
}
