package com.devcamp.cruddrinks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.cruddrinks.model.CMenu;


public interface IMenuRepository extends JpaRepository<CMenu, Long>{
    
}
